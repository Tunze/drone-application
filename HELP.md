
# Drone Application

Drone Application assignment from Musala Soft

# Read Me First


# Getting Started

##STEP 1

clone repository from gitlab using this command:

```
git clone https://gitlab.com/Tunze/drone-application.git
```

##STEP 2
 open with any java code editor of your choice then you can run it using two different method

* Using Docker
* on your editor.

#USING DOCKER 
NB : before running gradle build kindly check the version of your java becasuse the project is built on java 17, so you might have to change the version from gradle.build in the sourceCompatibility = [your java version]

```
1. run ./gradlew build

2. run docker build -t [repository-name-of-choice] .
```

Incase you come across unauthorised error run this command

```
 run sudo chmod 666 /var/run/docker.sock.
```

To start the drone application on docker
```
3. run docker run -p 8080:8080 [your-image-name-that-was-created-i-step1-above]
```

If if your image did not run as expected try this command

```
4. run docker ps -a
```
look for your image repository you created then on that row you will find the name of your image under NAMES when that is located run this command

```
5. run docker start [Name of image you located on the row] /
   run docker restart [Name of image you located on the row]
```




##USING YOUR EDITOR

1.run Droneapplication class which serves as main class/entry point of the project it can found in the root folder of com.musalasoft.droneapplication

the default application will run on port 8080

on your browser to view the API documentation on swagger enter
http://localhost:8080/swagger-ui/index.html#

Also to view database enter this link on your web below
http://localhost:8080/h2-console


To access the database change jdbc url to :

JDBC URL : jdbc:h2:mem:dronedb

password to the database is : password

#USER STORY

Initially there preloaded data that can be found when you run the database.

* Drone table
* location table

1. Register a drone with the api/v1/drone/register

2. load a drone with the api/v1/drone/load-drone

3. check drone batter level with the  api/v1/drone/battery-level/{droneId}

4. check available drone before loading up a drone this will give the user list of drones with few conditions that are met 

 api/v1/drone/available

   * drones that are not deleted.
   * drones whose battery are not less tha 25%
   * drones whose state is on IDLE







##EXTRAS

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.6.4/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.6.4/gradle-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.6.4/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.6.4/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)



