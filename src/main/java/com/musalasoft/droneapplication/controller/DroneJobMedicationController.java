package com.musalasoft.droneapplication.controller;

import com.musalasoft.droneapplication.dto.DroneMedicationResponse;
import com.musalasoft.droneapplication.service.DroneJobService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/api/v1/drone-job")
@RequiredArgsConstructor
public class DroneJobMedicationController {

    private final DroneJobService droneJobService;

    @PostMapping(value = "/get-drone-medication-item/{droneId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    DroneMedicationResponse getLoadedMedicationOnDrone(@PathVariable UUID droneId) {
        return droneJobService.getLoadedMedicationOnDrone(droneId);

    }


}
