package com.musalasoft.droneapplication.controller;

import com.musalasoft.droneapplication.dto.LoadDroneDto;
import com.musalasoft.droneapplication.dto.LoadDroneRequest;
import com.musalasoft.droneapplication.dto.RegisterDroneDto;
import com.musalasoft.droneapplication.dto.RegisterDroneRequest;
import com.musalasoft.droneapplication.model.Drone;
import com.musalasoft.droneapplication.service.DroneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/api/v1/drone")
@RequiredArgsConstructor
public class DroneController {

    private final DroneService droneService;

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    RegisterDroneDto registerADrone(@RequestBody @Validated RegisterDroneRequest request) {
        return RegisterDroneDto.fromDrone(droneService.registerADrone(request));
    }

    @GetMapping(value = "/available", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<Drone> searchForAvailableDrones() {
        return droneService.searchForAvailableDrones();
    }

    @GetMapping(value = "/battery-level/{droneId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String checkDroneBatteryLevel(@PathVariable UUID droneId) {

        return droneService.checkDroneBatteryLevel(droneId);
    }

    @PostMapping(value = "/load-drone", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    LoadDroneDto loadDrone(@ModelAttribute @Validated LoadDroneRequest request, @RequestPart("image") MultipartFile image)
    {
        return droneService.loadDrone(request, image);
    }


}
