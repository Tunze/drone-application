package com.musalasoft.droneapplication.service;

import com.musalasoft.droneapplication.dto.LoadDroneDto;
import com.musalasoft.droneapplication.dto.LoadDroneRequest;
import com.musalasoft.droneapplication.dto.RegisterDroneRequest;
import com.musalasoft.droneapplication.model.Drone;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

public interface DroneService {

    Drone registerADrone (RegisterDroneRequest request);

    List<Drone> searchForAvailableDrones();

    String checkDroneBatteryLevel(UUID droneId);

    LoadDroneDto loadDrone(LoadDroneRequest request, MultipartFile image);

    List<Drone> findAllByDeletedFalse();





}
