package com.musalasoft.droneapplication.service;

import com.musalasoft.droneapplication.model.Location;

import java.util.Optional;
import java.util.UUID;

public interface LocationService {

    Optional<Location> findLocationsByStartLocationAndAndDestinationLocation(String start, String end);

    Optional<Location> findById(UUID locationId);
}
