package com.musalasoft.droneapplication.service;

import com.musalasoft.droneapplication.dto.MedicationRequest;
import com.musalasoft.droneapplication.model.Medication;
import org.springframework.web.multipart.MultipartFile;

public interface MedicationService {

    Medication createMedicationRequest (MedicationRequest request, MultipartFile image);

}
