package com.musalasoft.droneapplication.service.impl;

import com.musalasoft.droneapplication.dto.MedicationRequest;
import com.musalasoft.droneapplication.imagemanagement.service.ImageUploadService;
import com.musalasoft.droneapplication.model.Medication;
import com.musalasoft.droneapplication.repository.MedicationRepository;
import com.musalasoft.droneapplication.service.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class MedicationServiceImplementation implements MedicationService {
    private final MedicationRepository medicationRepository;

    private final ImageUploadService imageUploadService;


    @Override
    public Medication createMedicationRequest(MedicationRequest request,  MultipartFile image) {

        var imageUpload = imageUploadService.saveImage(image, String.format("%s.%s", request.getMedicationName(), request.getMedicationCode()));


        var medications = Medication.builder()
                .medicationName(request.getMedicationName())
                .weight(request.getMedicationWeight())
                .imageUpload(imageUpload)
                .code(request.getMedicationCode())
                .build();

        return medicationRepository.save(medications);

    }
}
