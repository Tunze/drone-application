package com.musalasoft.droneapplication.service.impl;

import com.musalasoft.droneapplication.model.Location;
import com.musalasoft.droneapplication.repository.LocationRepository;
import com.musalasoft.droneapplication.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class LocationServiceImplementation implements LocationService {

    private final LocationRepository locationRepository;


    @Override
    public Optional<Location> findLocationsByStartLocationAndAndDestinationLocation(String start, String end) {
        return locationRepository.findLocationsByStartLocationAndAndDestinationLocation(start, end);
    }

    @Override
    public Optional<Location> findById(UUID locationId) {
        return locationRepository.findById(locationId);
    }
}
