package com.musalasoft.droneapplication.service.impl;

import com.musalasoft.droneapplication.enums.AuditType;
import com.musalasoft.droneapplication.model.AuditLog;
import com.musalasoft.droneapplication.model.Drone;
import com.musalasoft.droneapplication.repository.AuditLogRepository;
import com.musalasoft.droneapplication.service.DroneService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuditLogServiceImplementation {

    private final AuditLogRepository auditLogRepository;
    private final DroneService droneService;


    @Scheduled(fixedRateString ="${jobs.audit-battery-level.fixed-rate}")
    public void batteryAuditLog()
    {
        var droneList = droneService.findAllByDeletedFalse();

        for(Drone drone : droneList)
        {
            var battery = Integer.toString(drone.getBatteryCapacity()).concat(" %");
            var droneSerialNumber = drone.getSerialNumber();

            var description =  String.format("The Drone with the serial number %s battery level is %s as at %s", droneSerialNumber, battery, LocalDateTime.now());

            var audit = new AuditLog();

            audit.setDescription(description);
            audit.setCreateAt(LocalDateTime.now());
            audit.setAuditType(AuditType.BATTERY);

            auditLogRepository.save(audit);

        }

    }
}
