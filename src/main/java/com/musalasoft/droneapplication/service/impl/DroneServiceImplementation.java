package com.musalasoft.droneapplication.service.impl;

import com.musalasoft.droneapplication.dto.LoadDroneDto;
import com.musalasoft.droneapplication.dto.LoadDroneRequest;
import com.musalasoft.droneapplication.dto.MedicationRequest;
import com.musalasoft.droneapplication.dto.RegisterDroneRequest;
import com.musalasoft.droneapplication.enums.DroneModelType;
import com.musalasoft.droneapplication.enums.DroneStateType;
import com.musalasoft.droneapplication.exceptions.BadRequestException;
import com.musalasoft.droneapplication.exceptions.NotFoundException;
import com.musalasoft.droneapplication.model.Drone;
import com.musalasoft.droneapplication.model.DroneJob;
import com.musalasoft.droneapplication.repository.DroneRepository;
import com.musalasoft.droneapplication.service.DroneJobService;
import com.musalasoft.droneapplication.service.DroneService;
import com.musalasoft.droneapplication.service.LocationService;
import com.musalasoft.droneapplication.service.MedicationService;
import com.musalasoft.droneapplication.util.Util;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class DroneServiceImplementation implements DroneService {

    private final DroneRepository droneRepository;
    private final LocationService locationService;
    private final MedicationService medicationService;
    private final DroneJobService droneJobService;



    private static final String DRONE_DOES_NOT_EXIST = "Drone does not exist";

    @Override
    public Drone registerADrone(RegisterDroneRequest request) {

        var droneModel = validateDroneWeight(request.getWeightInGram());

        if (request.getWeightInGram() > 500) {
            throw new BadRequestException("The maximum drone weight allowed is 500gram");
        }


        var drone = new Drone();

        drone.setSerialNumber(request.getDroneSerialNumber());
        drone.setDroneModelType(droneModel);
        drone.setBatteryCapacity(request.getBatteryCapacity());
        drone.setWeightInGram(request.getWeightInGram());

        return droneRepository.save(drone);

    }

    @Override
    public List<Drone> searchForAvailableDrones() {
        return droneRepository.findAllByDroneStateTypeAndBatteryCapacityIsGreaterThanEqualAndDeletedFalse(DroneStateType.IDLE, 25);
    }

    @Override
    public String checkDroneBatteryLevel(UUID droneId) {
        var checkBattery = droneRepository.findById(droneId)
                .orElseThrow(() -> new NotFoundException(DRONE_DOES_NOT_EXIST));

        var battery = checkBattery.getBatteryCapacity();

        return String.valueOf(battery).concat("%");

    }

    @Override
    public LoadDroneDto loadDrone(LoadDroneRequest request, MultipartFile image) {

        var location = locationService.findLocationsByStartLocationAndAndDestinationLocation(request.getYourLocation(), request.getDeliveryLocation())
                .orElseThrow(() -> new NotFoundException("Sorry drones can't get to this location at the moment try another location"));

        var droneState = checkDroneState(request.getDroneId());

        if (droneState.equals(DroneStateType.LOADING) || droneState.equals(DroneStateType.LOADED)) {
            throw new BadRequestException("The drone requested is not available at the moment");
        }


        var drone = changeDroneState(request.getDroneId(), DroneStateType.LOADING);

        if (request.getMedicationWeight() > drone.getWeightInGram()) {
            throw new BadRequestException("The weight of your medication package is too heavy for this drone");
        }

        var medicationName = Util.sanitizeString(request.getMedicationName());
        var medicationCode = Util.sanitizeString2(request.getMedicationCode());

        var medicationRequestBody = MedicationRequest.builder()
                .medicationCode(medicationCode)
                .medicationName(medicationName)
                .medicationWeight(request.getMedicationWeight())
                .build();


        var medications = medicationService.createMedicationRequest(medicationRequestBody, image);


        var droneJobs = DroneJob.builder()
                .drone(drone)
                .locationId(location.getId())
                .medication(medications)
                .build();

        changeDroneState(drone.getId(), DroneStateType.LOADED);

        droneJobService.save(droneJobs);

        return LoadDroneDto.builder()
                .duration(location.getDurationInSeconds().toString().concat(" seconds"))
                .droneEstimatedBatteryForTrip(Integer.toString(location.getBatteryCapacity()).concat(" %"))
                .drone(droneJobs.getDrone().getSerialNumber())
                .startLocation(location.getStartLocation())
                .destinationLocation(location.getDestinationLocation())
                .medicationName(droneJobs.getMedication().getMedicationName())
                .medicationCode(droneJobs.getMedication().getCode())
                .medicationImage(droneJobs.getMedication().getImageUpload())
                .medicationWeight(droneJobs.getMedication().getWeight())
                .droneStateType(droneJobs.getDrone().getDroneStateType())
                .build();


    }

    @Override
    public List<Drone> findAllByDeletedFalse() {
        return droneRepository.findAllByDeletedFalse();
    }


    @Scheduled(initialDelayString = "${jobs.Update-drone-state-job.initial-delay}", fixedRateString = "${jobs.Update-drone-state-job.fixed-rate}")
    public void updateDroneStateStatus() {

        var droneList = droneRepository.findAllByDroneStateType(DroneStateType.LOADED);


        for (Drone drone : droneList) {
            if (drone.getDroneStateType().equals(DroneStateType.LOADED)) {
                log.info("state found");
                var departureTime = droneJobService.findByDrone(drone).orElseThrow(() -> new NotFoundException(DRONE_DOES_NOT_EXIST));
                drone.setDroneStateType(DroneStateType.DELIVERING);

                departureTime.setDepartureTime(LocalDateTime.now());

                droneJobService.save(departureTime);
                droneRepository.save(drone);
            }

        }

    }

    @Scheduled(fixedRateString = "${jobs.delivering-time.fixed-rate}")
    public void deliverMedication() {
        var droneList = droneRepository.findAllByDroneStateType(DroneStateType.DELIVERING);
        for (Drone drone : droneList) {
            var droneJob = droneJobService.findByDrone(drone).orElseThrow(() -> new NotFoundException(DRONE_DOES_NOT_EXIST));

            var location = droneJob.getLocationId();

            var departureTime = droneJob.getDepartureTime();

            var duration = locationService.findById(location).orElseThrow(() -> new NotFoundException("location does not exist")).getDurationInSeconds();

            var actualDeliveryTime = departureTime.plusSeconds(duration);

            if (LocalDateTime.now().isAfter(actualDeliveryTime)) {
                drone.setDroneStateType(DroneStateType.DELIVERED);
                droneJob.setDeliveryTime(LocalDateTime.now());

                droneJobService.save(droneJob);
                droneRepository.save(drone);
            }

        }
    }

    @Scheduled(fixedRateString ="${jobs.return-drone-time.fixed-rate}")
    public void returningDrone() {
        var droneList = droneRepository.findAllByDroneStateType(DroneStateType.DELIVERED);

        for (Drone drone : droneList) {

            drone.setDroneStateType(DroneStateType.RETURNING);

            droneRepository.save(drone);
        }


    }

    @Scheduled(fixedRateString ="${jobs.return-drone-time.fixed-rate}")
    public void returnDrone(){
        var droneList = droneRepository.findAllByDroneStateType(DroneStateType.RETURNING);

        for (Drone drone : droneList) {

            var droneJob = droneJobService.findByDrone(drone).orElseThrow(() -> new NotFoundException(DRONE_DOES_NOT_EXIST));

            var locationID = droneJob.getLocationId();

            var location = locationService.findById(locationID).orElseThrow(() -> new NotFoundException("location does not exist"));

            var deliveryTime = droneJob.getDeliveryTime();

            var returnTime = deliveryTime.plusSeconds(location.getDurationInSeconds());

            if(LocalDateTime.now().isAfter(returnTime))
            {
                var updateDroneBattery = drone.getBatteryCapacity() - location.getBatteryCapacity();
                drone.setDroneStateType(DroneStateType.IDLE);
                drone.setBatteryCapacity(updateDroneBattery);
            }
            droneRepository.save(drone);

        }

    }




    Drone changeDroneState(UUID droneId, DroneStateType droneStateType) {
        var drone = droneRepository.findById(droneId).orElseThrow(() -> new NotFoundException(DRONE_DOES_NOT_EXIST));

        drone.setDroneStateType(droneStateType);

        return droneRepository.save(drone);

    }

    DroneStateType checkDroneState(UUID droneId) {
        var droneState = droneRepository.findById(droneId).orElseThrow(() -> new NotFoundException(DRONE_DOES_NOT_EXIST));

        return droneState.getDroneStateType();
    }


    DroneModelType validateDroneWeight(int weight) {
        if (weight <= 100) {
            return DroneModelType.LIGHTWEIGHT;
        } else if (weight <= 150) {
            return DroneModelType.MIDDLEWEIGHT;
        } else if (weight <= 300) {
            return DroneModelType.CRUISERWEIGHT;
        } else {
            return DroneModelType.HEAVYWEIGHT;
        }
    }


}
