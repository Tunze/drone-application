package com.musalasoft.droneapplication.service.impl;

import com.musalasoft.droneapplication.dto.DroneMedicationResponse;
import com.musalasoft.droneapplication.exceptions.NotFoundException;
import com.musalasoft.droneapplication.model.Drone;
import com.musalasoft.droneapplication.model.DroneJob;
import com.musalasoft.droneapplication.repository.DroneJobRepository;
import com.musalasoft.droneapplication.service.DroneJobService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DroneJobImplementation implements DroneJobService {

    private final DroneJobRepository droneJobRepository;

    @Override
    public void save(DroneJob droneJob) {
        droneJobRepository.save(droneJob);
    }

    @Override
    public Optional<DroneJob> findByDrone(Drone drone) {
        return droneJobRepository.findByDrone(drone);
    }

    @Override
    public DroneMedicationResponse getLoadedMedicationOnDrone(UUID droneId) {
        var droneJob = droneJobRepository.findByDroneId(droneId).orElseThrow(()-> new NotFoundException("Drone does not exist or not assigned a job yet"));

        return DroneMedicationResponse.builder()
                .droneId(droneJob.getDrone().getId())
                .droneSerialNumber(droneJob.getDrone().getSerialNumber())
                .medicationName(droneJob.getMedication().getMedicationName())
                .code(droneJob.getMedication().getCode())
                .weight(droneJob.getMedication().getWeight())
                .build();
    }
}
