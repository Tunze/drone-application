package com.musalasoft.droneapplication.service;

import com.musalasoft.droneapplication.dto.DroneMedicationResponse;
import com.musalasoft.droneapplication.model.Drone;
import com.musalasoft.droneapplication.model.DroneJob;

import java.util.Optional;
import java.util.UUID;

public interface DroneJobService {

    void save (DroneJob droneJob);

    Optional<DroneJob> findByDrone(Drone drone);

    DroneMedicationResponse getLoadedMedicationOnDrone(UUID droneId);

}
