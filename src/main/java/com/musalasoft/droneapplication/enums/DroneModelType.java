package com.musalasoft.droneapplication.enums;

public enum DroneModelType {
    LIGHTWEIGHT("lightweight"),
    MIDDLEWEIGHT("middleweight"),
    CRUISERWEIGHT("cruiserweight"),
    HEAVYWEIGHT("heavyweight");

    DroneModelType( String weight){
        this.weight = weight;
    }

    private final String weight;

    @Override
    public String toString()
    {
        return weight;
    }


}
