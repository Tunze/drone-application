package com.musalasoft.droneapplication.enums;

public enum AuditType {
    BATTERY, MEDICATION
}
