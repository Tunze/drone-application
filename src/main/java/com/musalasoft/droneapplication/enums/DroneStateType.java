package com.musalasoft.droneapplication.enums;

public enum DroneStateType {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
