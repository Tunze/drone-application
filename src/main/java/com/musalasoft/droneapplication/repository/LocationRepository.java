package com.musalasoft.droneapplication.repository;

import com.musalasoft.droneapplication.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface LocationRepository extends JpaRepository<Location, UUID> {

    Optional<Location> findLocationsByStartLocationAndAndDestinationLocation(String start, String end);

    Optional<Location> findById(UUID locationId);

}
