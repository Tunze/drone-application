package com.musalasoft.droneapplication.repository;

import com.musalasoft.droneapplication.enums.DroneStateType;
import com.musalasoft.droneapplication.model.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface DroneRepository extends JpaRepository<Drone, UUID> {

    List<Drone> findAllByDroneStateTypeAndBatteryCapacityIsGreaterThanEqualAndDeletedFalse(DroneStateType droneStateType, int batteryCapacity);

    List<Drone> findAllByDroneStateType(DroneStateType droneStateType);

    List<Drone> findAllByDeletedFalse();



}
