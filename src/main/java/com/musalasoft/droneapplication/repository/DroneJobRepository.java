package com.musalasoft.droneapplication.repository;

import com.musalasoft.droneapplication.model.Drone;
import com.musalasoft.droneapplication.model.DroneJob;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface DroneJobRepository extends JpaRepository<DroneJob, UUID> {

    Optional<DroneJob> findByDrone(Drone drone);

    Optional<DroneJob> findByDroneId (UUID droneId);


}
