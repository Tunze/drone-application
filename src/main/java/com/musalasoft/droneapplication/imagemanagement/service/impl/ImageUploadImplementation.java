package com.musalasoft.droneapplication.imagemanagement.service.impl;

import com.musalasoft.droneapplication.imagemanagement.model.ImageUpload;
import com.musalasoft.droneapplication.imagemanagement.repository.ImageRepository;
import com.musalasoft.droneapplication.imagemanagement.service.ImageStorageService;
import com.musalasoft.droneapplication.imagemanagement.service.ImageUploadService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
public class ImageUploadImplementation implements ImageUploadService {

    private final ImageRepository imageRepository;

    private final ImageStorageService imageStorageService;


    @Override
    @SneakyThrows
    public ImageUpload saveImage(final MultipartFile file, final String fileName) {

            var location = imageStorageService.storeImage(file, fileName);

            var image = ImageUpload.builder()
                    .name(fileName)
                    .location(location)
                    .type(file.getContentType())
                    .build();

            return imageRepository.save(image);

    }
}
