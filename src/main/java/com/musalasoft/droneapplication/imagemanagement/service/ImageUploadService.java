package com.musalasoft.droneapplication.imagemanagement.service;

import com.musalasoft.droneapplication.imagemanagement.model.ImageUpload;
import org.springframework.web.multipart.MultipartFile;

public interface ImageUploadService {

    ImageUpload saveImage(MultipartFile file, String fileName);

}
