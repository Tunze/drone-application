package com.musalasoft.droneapplication.imagemanagement.service.impl;

import com.musalasoft.droneapplication.config.ImageStorageProperty;
import com.musalasoft.droneapplication.imagemanagement.exceptions.ImageStorageException;
import com.musalasoft.droneapplication.imagemanagement.service.ImageStorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Slf4j
@Service
public class ImageStorageImplementation implements ImageStorageService {


    private final Path imageStorageLocation;

    @Autowired
    public ImageStorageImplementation(ImageStorageProperty imageStorageProperty) {
        this.imageStorageLocation = Paths.get(imageStorageProperty.getUploadDirectory()).toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.imageStorageLocation);
        } catch (Exception ex) {
            throw new ImageStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }


    @Override
    public String storeImage(MultipartFile imageFile, String fileName) {
        String cleanName = StringUtils.cleanPath(fileName);

        try {
            if (cleanName.contains("..")) {
                throw new ImageStorageException("Sorry! Filename contains invalid path sequence" + cleanName);
            }

            Path targetLocation = this.imageStorageLocation.resolve(cleanName);
            Files.copy(imageFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return cleanName;


        } catch (IOException ex) {
            throw new ImageStorageException("Could not store file " + cleanName + ". Please try again!", ex);
        }
    }


}

