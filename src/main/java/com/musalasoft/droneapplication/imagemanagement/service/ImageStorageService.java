package com.musalasoft.droneapplication.imagemanagement.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageStorageService {

    String storeImage(MultipartFile imageFile, String fileName) throws IOException;
}
