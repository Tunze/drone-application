package com.musalasoft.droneapplication.imagemanagement.repository;

import com.musalasoft.droneapplication.imagemanagement.model.ImageUpload;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface ImageRepository extends JpaRepository<ImageUpload, UUID> {
}
