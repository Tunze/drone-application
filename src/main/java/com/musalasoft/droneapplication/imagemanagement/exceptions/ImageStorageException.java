package com.musalasoft.droneapplication.imagemanagement.exceptions;

public class ImageStorageException extends RuntimeException{

    public ImageStorageException(String message)
    {
        super(message);
    }

    public ImageStorageException(String message, Throwable cause)
    {
        super(message, cause);
    }

}
