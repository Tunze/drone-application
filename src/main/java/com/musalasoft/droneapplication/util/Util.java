package com.musalasoft.droneapplication.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Util {

    public static String sanitizeString(String value)
    {
        return value.replaceAll("[^A-Za-z0-9_-]", "").trim();
    }

    public static String sanitizeString2(String value)
    {
        return value.replaceAll("[^A-Z0-9_]", "").trim();
    }

}
