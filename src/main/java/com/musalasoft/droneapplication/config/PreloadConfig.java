package com.musalasoft.droneapplication.config;

import com.musalasoft.droneapplication.enums.DroneModelType;
import com.musalasoft.droneapplication.enums.DroneStateType;
import com.musalasoft.droneapplication.model.Drone;
import com.musalasoft.droneapplication.model.Location;
import com.musalasoft.droneapplication.repository.DroneRepository;
import com.musalasoft.droneapplication.repository.LocationRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class PreloadConfig {
    @Bean
    public ApplicationRunner initializer (DroneRepository droneRepository){
        return args -> droneRepository.saveAll(Arrays.asList(
                Drone.builder().serialNumber("5QRbU3OBY1ZQQ").batteryCapacity(100).droneModelType(DroneModelType.LIGHTWEIGHT).droneStateType(DroneStateType.IDLE).weightInGram(100).build(),
                Drone.builder().serialNumber("W21ab40yy1FFF").batteryCapacity(100).droneModelType(DroneModelType.MIDDLEWEIGHT).droneStateType(DroneStateType.IDLE).weightInGram(150).build(),
                Drone.builder().serialNumber("M80AKl0sxCl9m").batteryCapacity(100).droneModelType(DroneModelType.CRUISERWEIGHT).droneStateType(DroneStateType.IDLE).weightInGram(300).build(),
                Drone.builder().serialNumber("3Q4SvITz1qmPn").batteryCapacity(100).droneModelType(DroneModelType.HEAVYWEIGHT).droneStateType(DroneStateType.IDLE).weightInGram(500).build(),
                Drone.builder().serialNumber("P7ACBeY0TGLJw").batteryCapacity(100).droneModelType(DroneModelType.HEAVYWEIGHT).droneStateType(DroneStateType.IDLE).weightInGram(450).build()


        ));
    }

    @Bean
    public ApplicationRunner locationInitializer (LocationRepository locationRepository){
        return args -> locationRepository.saveAll(Arrays.asList(
                Location.builder().startLocation("Tsar Simeon").destinationLocation("Rakovska").durationInSeconds(50L).batteryCapacity(20).build(),
                Location.builder().startLocation("Graf Ignatiev").destinationLocation("Shishman").durationInSeconds(3L).batteryCapacity(1).build(),
                Location.builder().startLocation("Vitosha Boulevard").destinationLocation("Rakovska").durationInSeconds(10L).batteryCapacity(10).build(),
                Location.builder().startLocation("Lagos island").destinationLocation("ikeja").durationInSeconds(10L).batteryCapacity(10).build()

        ));
    }

}
