package com.musalasoft.droneapplication.model;

import com.musalasoft.droneapplication.enums.DroneModelType;
import com.musalasoft.droneapplication.enums.DroneStateType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "drones")

public class Drone extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "uuid")
    private UUID id;
    private String serialNumber;
    @Enumerated(EnumType.STRING)
    private DroneModelType droneModelType;
    private int weightInGram;
    private int batteryCapacity;
    @Enumerated(EnumType.STRING)
    private DroneStateType droneStateType = DroneStateType.IDLE;
    private boolean deleted = false;
}
