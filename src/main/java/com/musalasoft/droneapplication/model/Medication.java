package com.musalasoft.droneapplication.model;

import com.musalasoft.droneapplication.imagemanagement.model.ImageUpload;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Getter
@Setter
@Table(name = "medications")
public class Medication extends AuditModel{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String medicationName;
    private int weight;
    private String code;

    @OneToOne
    @JoinColumn(name="image_upload_id")
    private ImageUpload imageUpload;

}
