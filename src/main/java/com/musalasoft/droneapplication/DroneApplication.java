package com.musalasoft.droneapplication;

import com.musalasoft.droneapplication.config.ImageStorageProperty;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableConfigurationProperties({ImageStorageProperty.class})
@EnableJpaAuditing
public class DroneApplication {
	public static void main(String[] args) {
		SpringApplication.run(DroneApplication.class, args);
	}

}


