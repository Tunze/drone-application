package com.musalasoft.droneapplication.dto;

import com.musalasoft.droneapplication.enums.DroneStateType;
import com.musalasoft.droneapplication.imagemanagement.model.ImageUpload;
import lombok.Builder;
import lombok.Data;

@Data
@Builder

public class LoadDroneDto {
    private String duration;
    private String droneEstimatedBatteryForTrip;
    private String startLocation;
    private String destinationLocation;
    private String drone;
    private DroneStateType droneStateType;
    private String medicationName;
    private int medicationWeight;
    private String medicationCode;
    private ImageUpload medicationImage;


}
