package com.musalasoft.droneapplication.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class DroneMedicationResponse {

    private UUID droneId;
    private String droneSerialNumber;
    private String medicationName;
    private int weight;
    private String code;

}
