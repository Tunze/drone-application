package com.musalasoft.droneapplication.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class LoadDroneRequest {
    private String yourLocation;
    private String deliveryLocation;
    private UUID droneId;
    private String medicationName;
    private int medicationWeight;
    private String medicationCode;
}
