package com.musalasoft.droneapplication.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
public class RegisterDroneRequest {
    @Size(min = 5, max = 100, message = "serial number cannot be greater than 100 or less than 5 characters")
    @NotBlank(message = "drone serial Number cannot be empty")
    private String droneSerialNumber;
    @NotNull (message = "kindly add a drone weight ")
    private int weightInGram;
    @NotNull (message = "Battery Capacity can not be empty")
    private int batteryCapacity;
}
