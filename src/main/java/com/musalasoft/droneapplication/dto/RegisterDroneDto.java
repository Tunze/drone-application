package com.musalasoft.droneapplication.dto;

import com.musalasoft.droneapplication.enums.DroneModelType;
import com.musalasoft.droneapplication.enums.DroneStateType;
import com.musalasoft.droneapplication.model.Drone;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Data
public class RegisterDroneDto {
    private UUID id;
    private String serialNumber;
    private DroneModelType droneModelType;
    private int weight;
    private int batteryCapacity;
    private DroneStateType droneStateType;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;



    public static RegisterDroneDto fromDrone(final Drone drone)
    {
        return RegisterDroneDto.builder()
                .id(drone.getId())
                .serialNumber(drone.getSerialNumber())
                .droneModelType(drone.getDroneModelType())
                .droneStateType(drone.getDroneStateType())
                .batteryCapacity(drone.getBatteryCapacity())
                .weight(drone.getWeightInGram())
                .createdAt(drone.getCreatedAt())
                .updatedAt(drone.getUpdatedAt())
                .build();
    }


}
