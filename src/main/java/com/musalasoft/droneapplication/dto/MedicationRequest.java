package com.musalasoft.droneapplication.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class MedicationRequest {
    private String medicationName;
    private int medicationWeight;
    private String medicationCode;
}
