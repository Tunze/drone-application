# Start with a base image containing Java runtime
FROM openjdk:17

LABEL maintainer="tunde4excel@gmail.com"

VOLUME /tmp

EXPOSE 8080

ARG JAR_FILE=build/libs/drone-application-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} drone-app.jar

ENTRYPOINT ["java","-jar","/drone-app.jar"]